// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef HERVE_HPP_
#define HERVE_HPP_

/// \mainpage herve: a virtual reality library
///
/// static C++ library for developping virtual reality applications

#include <Herve/Chrono.hpp>
#include <Herve/FpsCounter.hpp>
#include <Herve/InputMotion.hpp>
#include <Herve/Random.hpp>
#include <Herve/Spline.hpp>
#include <Herve/Util.hpp>

// openal
#include <Herve/Sound.hpp>
#include <Herve/SoundFx.hpp>
#include <Herve/SoundFxReverb.hpp>

// opengl
#include <Herve/Camera.hpp>
#include <Herve/DisplayDevice.hpp>
#include <Herve/FrameBuffer.hpp>
#include <Herve/LoaderOBJ.hpp>
#include <Herve/Material.hpp>
#include <Herve/Mesh.hpp>
#include <Herve/Object.hpp>
#include <Herve/Scene.hpp>
#include <Herve/Shader.hpp>
#include <Herve/Texture.hpp>

// libvlc
#include <Herve/TextureVideo.hpp>


#endif
