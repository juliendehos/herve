# HERVE

C++ virtual reality library

[![demo on vimeo](image_herve.jpg)](https://vimeo.com/154497572)

## Features
- 3D rendering (OpenGL, GLSL)
- Wavefront OBJ loader
- image texture (libpng)
- video texture (libvlc)
- 3D sound (OpenAL, alure)
- Oculus Rift DK1 (linux only)
- animation: spline path
- navigation: walkthrough camera
- OS: linux, bsd

## Libraries
- libHerve: main library
- libHerveOvr: add oculus rift support (linux only)

## Demos
- Demo: skeleton application using sdl1
- DemoGlut: demo using glut (3D rendering, 3D sound, video)
- DemoGtkmm: demo using gtkmm 
- DemoSdl1: demo using sdl1 (3D rendering, 3D sound, video, oculus rift, gamepad...)

## Build & run
```
make -C libHerve
make -C libHerveOvr
make -C DemoSdl1
cd DemoSdl1/bin
./DemoSdl1
```

## License

[WTFPL](http://www.wtfpl.net) 

